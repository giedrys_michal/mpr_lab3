package com.mpr.repoPattern.db;

import java.util.List;

import com.mpr.repoPattern.domain.Order;


public interface OrderRepo extends Repo<Order> {
	
	public List<Order> findByClient(long clientId);
	public List<Order> findByAddress(long addressId);
}
