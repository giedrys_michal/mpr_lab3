package com.mpr.repoPattern.db;

import java.util.List;


public interface Repo<TEntity> {
	
	public TEntity findWithId(long id);
	public void add(TEntity entity);
	public void modify(TEntity entity);
	public void remove(TEntity entity);
	public List<TEntity> getAll();
}
