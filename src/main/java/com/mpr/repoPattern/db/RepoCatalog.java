package com.mpr.repoPattern.db;

public interface RepoCatalog {
	
	public OrderRepo orders();
}
