package com.mpr.repoPattern.db.catalogs;

import java.sql.Connection;

import com.mpr.repoPattern.db.OrderRepo;
import com.mpr.repoPattern.db.RepoCatalog;
import com.mpr.repoPattern.db.repos.HsqlOrderRepo;


public class HsqlRepoCatalog implements RepoCatalog{

	Connection connection;
	
	public HsqlRepoCatalog(Connection connection) {
		this.connection = connection;
	}

	public OrderRepo orders() {
		return new HsqlOrderRepo(connection);
	}

}
