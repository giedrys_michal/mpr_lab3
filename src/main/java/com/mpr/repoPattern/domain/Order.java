package com.mpr.repoPattern.domain;

import java.util.List;

public class Order {
	private long id;	
	private ClientDetails client;
	private Address deliveryAddress;
	private List<Item> items;
	
	public Order() {
	}
	
	public Order(long id, ClientDetails client, Address deliveryAddress, List<Item> items) {
		super();
		this.id = id;
		this.client = client;
		this.deliveryAddress = deliveryAddress;
		this.items = items;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public ClientDetails getClient() {
		return client;
	}
	public void setClient(ClientDetails client) {
		this.client = client;
	}
	public Address getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
}
