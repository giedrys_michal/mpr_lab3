package com.mpr.repoPattern.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mpr.repoPattern.domain.Item;
import com.mpr.repoPattern.domain.Mapping;

public class MappingManager {
	private Connection connection;
	private String url = "jdbc:hsqldb:hsql://localhost/work_db";
	
	private String createTableMapping = "CREATE TABLE Mapping(order_id bigint, item_id bigint)";
	
	private PreparedStatement addMappingStmt;
	private PreparedStatement getMappingStmt;
	private PreparedStatement deleteAllMappingsStmt;
	private PreparedStatement getAllMappingsStmt;
	
	private Statement statement;
	
	public MappingManager() {
		try {
			connection = DriverManager.getConnection(url, "SA", "");
			statement = connection.createStatement();

			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean tableExists = false;
			while (rs.next()) {
				if ("Mapping".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}

			if (!tableExists)
				statement.executeUpdate(createTableMapping);
			
			addMappingStmt = connection
					.prepareStatement("INSERT INTO Mapping (order_id, item_id) VALUES (?, ?)");
			
			deleteAllMappingsStmt = connection
					.prepareStatement("DELETE FROM Mapping");
			
			getAllMappingsStmt = connection
					.prepareStatement("SELECT order_id, item_id FROM Mapping");


		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public void clearMappings() {
		try {
			deleteAllMappingsStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int addMapping(Mapping mapping) {
		int count = 0;
		try {
			addMappingStmt.setLong(1, mapping.getOrder_id());
			addMappingStmt.setLong(2, mapping.getItem_id());
			
			count = addMappingStmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return count;
	}
	
	public List<Item> getMapping(long id) {
		List<Item> items = new ArrayList<Item>();
		ItemManager im = new ItemManager();
		
		try {
			getMappingStmt = connection
					.prepareStatement("SELECT order_id, item_id FROM Mapping WHERE order_id=" + id);
			ResultSet rs = getMappingStmt.executeQuery();
			
			while (rs.next()) {
				Item item = im.getItem(rs.getLong("item_id"));
				items.add(item);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return items;
	}
	
	public List<Mapping> getAllMappings() {
		List<Mapping> mappings = new ArrayList<Mapping>();
		
		try {
			ResultSet rs = getAllMappingsStmt.executeQuery();
			
			while (rs.next()) {
				Mapping m = new Mapping();
				
				m.setOrder_id(rs.getLong("order_id"));
				m.setItem_id(rs.getLong("item_id"));
				
				mappings.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mappings;
	}
}
